from django.db import models
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User


class skill(models.Model):
    nombre = models.CharField(max_length=100)
    slug = models.SlugField(editable = True, unique=True, null=True, blank=True, max_length=255)

    class Meta:
        verbose_name = "Skill"
        verbose_name_plural = "Skills"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(skill, self).save(*args,**kwargs)

    def __str__(self):
        return self.nombre

def upload_imagen_to(instance, filename):
    import os
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    return 'fotos-tecnicos/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(), 
    )

class Tecnico(User):

    cedula = models.IntegerField(null=True,unique=True)
    telefono = models.BigIntegerField(null=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    direccion = models.CharField(max_length=150,blank=True)
    perfil_profesional = models.TextField(blank=False)
    nivel = models.IntegerField(default=1,null=False)
    exp = models.IntegerField(default=0)
    puntos = models.IntegerField(default=0,null=True)
    skills = models.ManyToManyField(skill,blank=True)
    foto = models.ImageField(upload_to=upload_imagen_to, blank=True)
 
    DISPONIBLE='Disponible'
    OCUPADO='Ocupado'
    Fuera= 'Fuera de Servicio'
    Online= 'EN Línea'

    disponibilidad = (
        (DISPONIBLE, 'Disponible'),
        (OCUPADO, 'Ocupado'),
        (Fuera, 'Fuera de Servicio'),
        (Online, 'EN Línea'),
        )
    estado = models.CharField(max_length=20,choices=disponibilidad,default= Fuera)

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_active = False
        super(Tecnico, self).save(*args, **kwargs)
    

    class Meta:
    	verbose_name = 'Tecnico'
    	verbose_name_plural = 'Tecnicos'


    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)