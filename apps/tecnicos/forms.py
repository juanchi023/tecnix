from django import forms
from django.contrib.auth import authenticate 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from apps.tecnicos.models import Tecnico


class TecnicoProfileForm(forms.ModelForm):
    fecha_nacimiento=forms.DateField(widget=forms.widgets.DateInput(format="%m-%d-%Y"), input_formats=["%m-%d-%Y"])

    class Meta:
        model = Tecnico
        exclude = (
            "skills",
            "nivel",
            "puntos",
            "estado",
            "exp",
            "password",
            "last_login",
            "groups",
            "user_permissions",
            "is_staff",
            "is_active",
            "is_superuser",
            "date_joined",)

    def __init__(self, *args, **kwargs):
        super(TecnicoProfileForm, self).__init__(*args, **kwargs)