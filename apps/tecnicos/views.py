from django.shortcuts import render
from django.contrib.auth import login,logout
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from . models import Tecnico
from .forms import TecnicoProfileForm
from tecnix.estandar.view import InstanceMixin, LoginMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.urlresolvers import reverse_lazy


class TecnicoUpdateView(UpdateView, LoginMixin):
    template_name = "app/editar-perfil-tecnicos.html"
    model = Tecnico
    form_class = TecnicoProfileForm
    success_url = reverse_lazy('profile')

    def get_object(self):
    	return self.request.user.tecnico
