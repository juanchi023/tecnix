from django.conf.urls import include, url
from apps.tecnicos import views as tecnicos_views


urlpatterns = [
	
	url(r'^panel/perfil/editar-perfil$', tecnicos_views.TecnicoUpdateView.as_view(), name='edit_perfil_tecnico'),
]