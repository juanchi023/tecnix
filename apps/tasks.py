from celery import shared_task
from celery.decorators import task
from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from tecnix.settings.celery import app
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.core.serializers.json import DjangoJSONEncoder


@app.task
def send_email_gracias(cliente,email,solicitud1,codigo_solicitud):

	htmly     = get_template('app/notificacion.html')

	d = Context({'cliente': cliente, 'email': email,'solicitud1':solicitud1
        ,'codigo_solicitud':codigo_solicitud})
	subject, from_email, to = 'Notificacion solicitud en Tecnix', 'hola@tecnix.co', email
	html_content = htmly.render(d)
	msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()


@app.task
def send_email_tecnicos(tecnicos_json):
	data  = json.loads(tecnicos_json)
	htmly     = get_template('app/notificacion-tec.html')

	for i in range(0, len(data)):

		d = Context({'tecnico': data[i][0]})
		subject, from_email, to = 'Notificacion solicitud en Tecnix', 'hola@tecnix.co', data[i][1]
		html_content = htmly.render(d)
		msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()




