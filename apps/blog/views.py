from django.shortcuts import render
from .models import Categoria, Noticia, Answer, Tutorial, Calificacion_tuto
from django.views.generic import ListView, CreateView, DetailView
from tecnix.estandar.view import InstanceMixin, LoginMixin
from .forms import NuevoPostForm, NuevaCategoriaForm, EditCategoriaForm, EditPostForm
from apps.home.models import Suscriptor
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
 

def home_blog(request):
    tutos = Tutorial.objects.all()
    categs = Categoria.objects.all()
    noticias = Noticia.objects.all()

    return render(request,'blog/index.html',{'noticias':noticias
		,'tutos':tutos,'categs':categs})

@require_POST 
def registrar_suscriptor(request):
    form = Suscriptor()
    if request.method == 'POST':
        items = request.POST.get('suscriptor_dict', None)
        items = str(items).split("/")

        for i in range(0, len(items)):
            item = json.loads(items[i])
            nombre = item['nombre']
            email = item['email']
        form.nombre = nombre
        form.email = email
        form.save()
        message = 'Su solicitud se ha registrado! Gracias por suscribirte.'
        ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')


class PostDetailView(DetailView):

    model = Noticia
    context_object_name = 'noticia'
    template_name ='blog/publicacion.html'

    def get_answers(self, noticia):
        answers = Answer.objects.filter(noticia = noticia).order_by('-created')

        noticia1 = Noticia.objects.get(slug = noticia.slug)
        noticia1.vistas += 1
        noticia1.save()
        return answers

    def get_context_data(self, *args, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['answers'] = self.get_answers(context['object'])
        context['noticias'] = Noticia.objects.all().order_by('-created')[:6]
        context['categorias'] = Categoria.objects.all().order_by('-nombre')
        
        return context

    def post(self, request, *args, **kwargs):
        answer = Answer()
        answer.user = request.POST['user']
        answer.description = request.POST['description']
        answer.noticia = Noticia.objects.get(slug = kwargs['slug'])
        answer.save()
        answers = self.get_answers(answer.noticia)
        return render(request, 'blog/publicacion.html', 
                        {'noticia' : answer.noticia , 'answers':answers})

        
class TutorialDetailView(DetailView):

    model = Tutorial
    context_object_name = 'tutorial'
    template_name ='blog/detalle-tutorial.html'

    def get_Calificacion_tuto(self, tutorial):
        calificaciones = Calificacion_tuto.objects.filter(tutorial = tutorial).order_by('-fecha')

        tutorial1 = Tutorial.objects.get(slug = tutorial.slug)
        tutorial1.vistas = tutorial1.vistas + 1
        tutorial1.save()
        print("sumé 1")
        return calificaciones

    def get_context_data(self, *args, **kwargs):
        context = super(TutorialDetailView, self).get_context_data(**kwargs)
        context['calificaciones'] = self.get_Calificacion_tuto(context['object'])
        context['tutoriales'] = Tutorial.objects.all().order_by('-created')[:6]
        context['categorias'] = Categoria.objects.all().order_by('-nombre')

        califiaciones2 = self.get_Calificacion_tuto(context['object'])
        suma = 0
        if califiaciones2:
        	for x in califiaciones2:
        		suma += x.puntuacion
        	context['calificacion_total'] = suma / califiaciones2.count()
        else:
        	context['calificacion_total'] = 0
        
        return context

    def post(self, request, *args, **kwargs):
        calificacion = Calificacion_tuto()
        if request.user.is_authenticated():
        	calificacion.usuario = request.user
        else:
        	calificacion.usuario = request.POST['user']
        
        calificacion.comentario = request.POST['description']
        calificacion.puntuacion = request.POST['calificacion']
        calificacion.tutorial = Tutorial.objects.get(slug = kwargs['slug'])
        calificacion.save()
        calificaciones = Calificacion_tuto.objects.filter(tutorial=calificacion.tutorial)

        suma = 0
        for x in calificaciones:
        	suma += x.puntuacion

        calificacion_total = suma / calificaciones.count()

        tutoriales = Tutorial.objects.all().order_by('-created')[:6]

        return render(request, 'blog/detalle-tutorial.html', 
                        {'tutorial' : calificacion.tutorial
                        , 'calificaciones':calificaciones
                        ,'calificacion_total':calificacion_total
                        ,'tutoriales':tutoriales})

class crear_post(CreateView, InstanceMixin,LoginMixin):

    template_name = "blog/crear-post.html"
    model = Noticia
    form_class = NuevoPostForm
    success_url = reverse_lazy('ver_posts')

    def form_valid(self, form):
    	form.instance.user = self.request.user
    	return super(crear_post, self).form_valid(form)

    def form_invalid(self, form):
    	return super(crear_post, self).form_invalid(form)


@login_required
def gestion_categorias(request):

    form = NuevaCategoriaForm(request.POST or None)
    categorias = Categoria.objects.all().order_by('nombre')

    if form.is_valid():
        form.save()
        
        return HttpResponseRedirect(reverse('gestion_categorias'))

    return render (request,'blog/gestion-categorias.html',{'form':form,'categorias':categorias})

class CategoriaUpdateView(UpdateView, LoginMixin):
    template_name = "blog/edit-categoria.html"
    model = Categoria
    form_class = EditCategoriaForm

    def get_success_url(self, **kwargs):

        return reverse_lazy('gestion_categorias')

class CategoriaDeleteView(DeleteView, LoginMixin):

    template_name = "blog/delete-cat.html"
    model = Categoria
    success_url = reverse_lazy('gestion_categorias')

def ver_posts(request):
	posts = Noticia.objects.all().order_by('-created')
	return render (request,'blog/ver-noticias.html',{'posts':posts})

class PostUpdateView(UpdateView, LoginMixin):
    template_name = "blog/edit-post.html"
    model = Noticia
    form_class = EditPostForm

    def get_success_url(self, **kwargs):

        return reverse_lazy('ver_posts')

class PostDeleteView(DeleteView, LoginMixin):

    template_name = "blog/delete-post.html"
    model = Noticia
    success_url = reverse_lazy('ver_posts')

class CategoriaView(ListView):
    template_name ='blog/reviews.html'
    context_object_name = "list"
    paginate_by = '10'

    def get_queryset(self, *args, **kwargs):
        noticias = Noticia.objects.filter(categoria__slug=self.kwargs['slug']).order_by('-created')      
        return noticias