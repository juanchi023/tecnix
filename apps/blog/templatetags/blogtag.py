from django import template

register = template.Library()


@register.filter
def get_count_noticias(noticia):

	num = noticia.count()

	return num

@register.filter
def get_count_comentarios(answer):

	num2 = answer.count()

	return num2

@register.filter
def get_count_calificaciones(calificacion_tuto):

	num2 = calificacion_tuto.count()

	return num2


	