from django.contrib import admin
from .models import Categoria, Noticia, Answer, Tutorial, Calificacion_tuto
from apps.home.models import Suscriptor


admin.site.register(Categoria)
admin.site.register(Noticia)
admin.site.register(Answer)
admin.site.register(Tutorial)
admin.site.register(Calificacion_tuto)
admin.site.register(Suscriptor)