from django.conf.urls import include, url
from apps.blog import views as blog_views

urlpatterns = [
	url(r'^blog/$', blog_views.home_blog
		, name='home_blog'),

	url(r'^blog/post/(?P<slug>[-\w]+)/$', blog_views.PostDetailView.as_view()
    	, name='detalle_post'),

	url(r'^blog/tutorial/(?P<slug>[-\w]+)/$', blog_views.TutorialDetailView.as_view()
    	, name='detalle_tutorial'),
	url(r'^panel/blog/crear-post/$', blog_views.crear_post.as_view(), name='crear_post'),
    url(r'^panel/blog/gestion-categorias/$', blog_views.gestion_categorias, name='gestion_categorias'),
    url(r'^panel/blog/gestion-categorias/editar/(?P<pk>\d+)/$', blog_views.CategoriaUpdateView.as_view(), name='editar_cat'),
    url(r'^panel/blog/gestion-categorias/borrar/(?P<pk>\d+)/$', blog_views.CategoriaDeleteView.as_view(), name='delete_cat'),
    url(r'^panel/blog/ver-posts/$', blog_views.ver_posts, name='ver_posts'),
    url(r'^panel/blog/editar-post/(?P<pk>\d+)/$', blog_views.PostUpdateView.as_view(), name='editar_post'),
    url(r'^panel/blog/borrar-post/(?P<pk>\d+)/$', blog_views.PostDeleteView.as_view(), name='delete_post'),
    url(r'^blog/categoria/(?P<slug>[-\w]+)/$' ,blog_views.CategoriaView.as_view(), name = 'categoria-blog'),
    url(r'^registrar-suscriptor/$', blog_views.registrar_suscriptor, name='registrar_susciptor'),
	
	
  
]