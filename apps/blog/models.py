from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.encoding import python_2_unicode_compatible


class TimeStampModel(models.Model):

	user = models.ForeignKey(User, db_index=True, null=True, blank=True)
	description = models.TextField()
	created = models.DateTimeField(auto_now_add = True)
	modified = models.DateTimeField(auto_now = True)

	class Meta:
		abstract = True 

@python_2_unicode_compatible
class Categoria(models.Model):

	nombre = models.CharField(max_length=40)
	slug = models.SlugField(editable = False, unique=True, null=True, blank=True, max_length=255)

	def __str__(self):
		return "{0}".format(self.nombre)

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.nombre)
		super(Categoria, self).save(*args,**kwargs)
		
def upload_imagen_to(instance, filename):
    import os
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    return 'noticias/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(),
    )
@python_2_unicode_compatible
class Noticia(TimeStampModel):

	imagen = models.ImageField(upload_to=upload_imagen_to, blank=True)
	titulo = models.CharField(max_length=400, editable = True)
	categoria = models.ManyToManyField(Categoria)
	slug = models.SlugField(editable = True, unique=True, null=True, blank=True,max_length=400)
	vistas = models.BigIntegerField(default=0)
	def save(self, *args, **kwargs):
		self.slug = slugify(self.titulo)
		super(Noticia, self).save(*args,**kwargs)

	def __str__(self):
		return "{0}".format(self.titulo)




class Answer(models.Model):

	user = models.CharField(max_length=500, blank=False)
	description = models.TextField()
	created = models.DateTimeField(auto_now_add = True)
	noticia = models.ForeignKey(Noticia)

	def __str__(self):
		return "{0}".format(self.user)

def upload_imagen_tuto(instance, filename):
    import os
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    return 'tutoriales/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(),
    )

@python_2_unicode_compatible
class Tutorial(TimeStampModel):

	imagen = models.ImageField(upload_to=upload_imagen_tuto, blank=True)
	titulo = models.CharField(max_length=400, editable = True)
	categoria = models.ManyToManyField(Categoria)
	slug = models.SlugField(editable = True, unique=True
		, null=True, blank=True,max_length=400)
	video_id = models.CharField(max_length=50, editable = True, blank=True)
	vistas = models.BigIntegerField(default=0)


	def save(self, *args, **kwargs):
		self.slug = slugify(self.titulo)
		super(Tutorial, self).save(*args,**kwargs)

	def __str__(self):
		return "{0}".format(self.titulo)

class Calificacion_tuto(models.Model):
	usuario =  models.CharField(max_length=500, blank=False)
	tutorial = models.ForeignKey(Tutorial)
	fecha = models.DateTimeField(auto_now_add=True)
	puntuacion = models.IntegerField(default=1)
	comentario = models.TextField(blank=True)

	def __str__(self): 
		return "{0} - {1} - {2}".format(self.puntuacion,self.tutorial,self.usuario)