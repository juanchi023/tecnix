from __future__ import unicode_literals
from django.forms import ModelForm
from django import forms
from .models import Categoria, Noticia, Answer


class NuevoPostForm(forms.ModelForm):
	
	class Meta:
		model = Noticia
		exclude = ('created','modified','user','slug','vistas')

	def __init__(self, *args, **kwargs):
		super(NuevoPostForm, self).__init__(*args, **kwargs)

class EditPostForm(forms.ModelForm):
	
	class Meta:
		model = Noticia
		exclude = ('created','modified','user','slug','vistas')

	def __init__(self, *args, **kwargs):
		super(EditPostForm, self).__init__(*args, **kwargs)


class NuevaCategoriaForm(forms.ModelForm):
	
	class Meta:
		model = Categoria
		exclude = ('slug',)

class EditCategoriaForm(forms.ModelForm):
	
	class Meta:
		model = Categoria
		exclude = ('slug',)