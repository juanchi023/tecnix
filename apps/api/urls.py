from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers, serializers, viewsets
from .clientes import cliente_viewset as cliente_views
from .tecnicos import tecnico_viewset as tecnico_views
from rest_framework.authtoken import views
from apps.clientes.models import Cliente
from .clientes.cliente_serializer import ClienteSerializer
from .tecnicos.tecnico_serializer import TecnicoSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

router = routers.DefaultRouter()
router.register(r'clientes', UserViewSet)


urlpatterns = [
	url(r'^api/', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	url(r'^registro-usuarios-api/$', cliente_views.CreateClienteView.as_view(), name='register_user_api'),
	url(r'^registro-tecnicos-api/$', tecnico_views.CreateTecnicoView.as_view(), name='register_tec_api'),
	
  
]