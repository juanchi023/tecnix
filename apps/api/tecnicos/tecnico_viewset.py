from rest_framework.generics import CreateAPIView
from rest_framework import viewsets, generics
from apps.tecnicos.models import Tecnico
from .tecnico_serializer import TecnicoSerializer


class CreateTecnicoView(CreateAPIView):

    model = Tecnico
    serializer_class = TecnicoSerializer
