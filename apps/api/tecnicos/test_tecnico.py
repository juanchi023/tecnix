from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from apps.tecnicos.models import Tecnico

"""
Cliente.objects.create(
           
            cedula=validated_data['cedula'],
            telefono=validated_data['telefono'],
            direccion=validated_data['direccion'],
"""

class UserTests(APITestCase):
    def test_create_user(self):
        
        url = reverse('register_tec_api')
        data = {'username': 'ywarezk','first_name': 'Yariv','last_name': 'Katz','password': '12345678','cedula': '1234','email': 'no@no.no','telefono': '1234','direccion': 'La Esperanza'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Tecnico.objects.count(), 1)

        #other requests are forbidden
        data = {'username': 'ywarezk','first_name': 'Yariv','last_name': 'Katz','password': '12345678','cedula': '1234','email': 'no@no.no','telefono': '1234','direccion': 'La Esperanza'}
        response = self.client.put(url + '/1', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        response = self.client.delete(url + '/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)