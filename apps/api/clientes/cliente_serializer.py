from rest_framework import serializers
from apps.clientes.models import Cliente
from django.contrib.auth.models import User
import sendgrid
import os
try:
    # Python 3
    import urllib.request as urllib
except ImportError:
    # Python 2
    import urllib2 as urllib
from tecnix.settings.base import SENDGRID_API_KEY


class ClienteSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    
    def create(self, validated_data):
        user = Cliente.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        nombre = '%s %s' % (validated_data['first_name'],validated_data['last_name'])
        email = email=validated_data['email']
        clave = validated_data['password']
        send_mail(nombre, email, clave)
        return user

    def validate_email(self, value):

        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError("Ya existe una cuenta vinculada a este email")

        return value

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance



    class Meta:
        model = Cliente
        fields = ('username', 'first_name', 'last_name', 'password', 'email',)
        write_only_fields = ('password',)
        read_only_fields = ('is_staff', 'is_superuser', 'is_active', 'date_joined',)

def send_mail(nombre,email,clave):

    sg = sendgrid.SendGridAPIClient(apikey=SENDGRID_API_KEY)
    data = {
      "personalizations": [
        {
          "to": [
            {
              "email": email
            }
          ],
          "substitutions": {
            "-cliente-": nombre,
            "-email-": email,
            "-password-": clave,
          },
        },
      ],
      "from": {
        "email": "hola@tecnix.co"
      },
      "content": [
        {
          "type": "text/html",
          "value": "I'm replacing the <strong>body tag</strong>"
        }
      ],
      "template_id": "017d389b-9a57-46b8-bff9-b90b0ee43fe2"
    }
    try:
        response = sg.client.mail.send.post(request_body=data)
    except urllib.HTTPError as e:
        print (e.read())
        exit()
    print(response.status_code)