from rest_framework.generics import CreateAPIView
from rest_framework import viewsets, generics
from apps.clientes.models import Cliente
from .cliente_serializer import ClienteSerializer


class CreateClienteView(CreateAPIView):

    model = Cliente
    serializer_class = ClienteSerializer
