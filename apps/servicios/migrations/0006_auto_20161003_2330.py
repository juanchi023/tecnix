# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-04 04:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0005_auto_20161002_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitud',
            name='direccion',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='solicitud',
            name='direccion_completa',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='solicitud',
            name='tipo_dispositivo',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
