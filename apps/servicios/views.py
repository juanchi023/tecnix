from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from .forms import create_solicitud_fom 
from django.http import HttpResponseRedirect, HttpResponse
from apps.servicios.models import Servicio, Solicitud
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import ListView, CreateView, DetailView
from django.views.decorators.http import require_POST
from datetime import datetime  
from apps.tecnicos.models import Tecnico
from apps.gamificacion.models import Calificacion
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.decorators import login_required
from tecnix.estandar.view import InstanceMixin, LoginMixin
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from apps.tasks import send_email_gracias, send_email_tecnicos
from django.core.serializers.json import DjangoJSONEncoder




class Listarsolicitudes(ListView,LoginMixin):

    template_name = 'app/ver-solicitudes.html'
    context_object_name = "list"

    def get_queryset(self):
        try:
            solicitudes = Solicitud.objects.filter(cliente=self.request.user.cliente).order_by('-fecha_solicitado')
        except:
            solicitudes = Solicitud.objects.filter(tecnico=self.request.user.tecnico).order_by('-fecha_solicitado')
        return solicitudes


@login_required
def create_solicitud(request, *args, **kwargs):
    form = create_solicitud_fom(request.POST or None)
    if form.is_valid():
        solic =form.save()
        solic.cliente = request.user.cliente
        solic.tipo_dispositivo = request.POST.get('device_type')
        solic.save()
        codigo = solic.codigo
        url = reverse('detalle_solicitud', kwargs={'codigo': codigo})
        tecnicos = Tecnico.objects.filter(is_active=True).values_list('first_name', 'email')
        tecnicos_json = json.dumps(list(tecnicos), cls=DjangoJSONEncoder)

        send_email_tecnicos.delay(tecnicos_json)  
        
        return HttpResponseRedirect(url)

    return render(request,'app/solicitud.html', {'form':form})
@login_required
def SolicitudDetailView(request, *args, **kwargs):

    solicitud = Solicitud.objects.get(codigo=kwargs.get("codigo"))

    try:
        calificacion = Calificacion.objects.get(solicitud=solicitud)
        return render(request,'app/detalle-solicitud.html', {'solicitud':solicitud
            ,'calificacion':calificacion})
    except:
        return render(request,'app/detalle-solicitud.html', {'solicitud':solicitud})

    
@login_required
@require_POST 
def confirmar_llegada_tecnico(request):
    if request.method == 'POST':

        codigo = request.POST.get('codigo', None)
        solicitud = get_object_or_404(Solicitud, codigo=codigo)
        solicitud.fecha_inicio = datetime.now()
        solicitud.save()

        message = 'Gracias por avisarnos que su técnico ha llegado. Estamos pendiente!'

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')
@login_required
@require_POST 
def confirmar_fin_servicio(request):
    if request.method == 'POST':

        codigo = request.POST.get('codigo', None)
        solicitud = get_object_or_404(Solicitud, codigo=codigo)
        solicitud.fecha_fin = datetime.now()
        solicitud.estado = 'Solicitud Cerrada'
        cliente = solicitud.cliente
        cliente.puntos = cliente.puntos + 120
        cliente.save()
        tecnico = solicitud.tecnico
        tecnico.exp = tecnico.exp + 20
        if tecnico.exp == 100:
            tecnico.nivel = tecnico.nivel + 1
            tecnico.exp = 0
        elif tecnico.exp >= 100:
            tecnico.exp = tecnico.exp -100
            tecnico.nivel = tecnico.nivel + 1
        tecnico.save()
        solicitud.save()

        message = 'Gracias, servicio ha terminado satisfactoriamente. ¡Has obtenido 100 puntos!'

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')

@login_required
@require_POST 
def tomar_servicio(request):
    if request.method == 'POST':

        items = request.POST.get('req_arr', None)
        items = str(items).split("/")
        for i in range(0, len(items)):
            item = json.loads(items[i])
            codigo = item['codigo']
            tecnico_id = item['tecnico_id']
     
        solicitud = get_object_or_404(Solicitud, codigo=codigo)
        tecnico1 = get_object_or_404(Tecnico, id=tecnico_id)
        
        if not solicitud.tecnico:
            solicitud.tecnico = tecnico1
            solicitud.estado = 'Tecnico Asignado'
            solicitud.save()
            cliente = solicitud.cliente.first_name + " " + solicitud.cliente.last_name
            email = solicitud.cliente.email
            solicitud1 = "["+solicitud.paquete.nombre + ": " + solicitud.servicio.nombre + " - " + solicitud.direccion + "]"
            codigo_solicitud = solicitud.codigo
            send_email_gracias.delay(cliente,email,solicitud1,codigo_solicitud)
            tecnico1.estado = 'Ocupado'
            tecnico1.save()
            message = 'Usted ha aceptado este servicio, porfavor diríjase a sus servicios ! Su estado ahora es: Ocupado'
        else:
            message = 'Ya han tomado este servicio'
       

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST 
def valor_servicio(request):
    if request.method == 'POST':

        option = request.POST.get('option', None)
        servicio = get_object_or_404(Servicio, id=option)
        

        message = servicio.precio

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')
@login_required
@require_POST 
def calificar_servicio(request):
    if request.method == 'POST':

        items = request.POST.get('req_arr', None)
        items = str(items).split("/")
        for i in range(0, len(items)):
            item = json.loads(items[i])
            codigo = item['codigo']
            tecnico_id = item['tecnico_id']
            calificacion = item['calificacion']
            comentario = item['comentario']
     
        solicitud = get_object_or_404(Solicitud, codigo=codigo)
        cliente =  solicitud.cliente
        tecnico = get_object_or_404(Tecnico, id=tecnico_id)
        form = Calificacion()
        form.cliente = solicitud.cliente
        form.solicitud = solicitud
        form.tecnico = tecnico
        form.puntuacion = int(calificacion)
        form. comentario = comentario
        form.save()
        tecnico.exp = tecnico.exp + (int(calificacion)*5)
        if tecnico.exp == 100:
            tecnico.nivel = tecnico.nivel + 1
            tecnico.exp = 0
        elif tecnico.exp >= 100:
            tecnico.exp = tecnico.exp -100
            tecnico.nivel = tecnico.nivel + 1
        tecnico.save()
        cliente.puntos = cliente.puntos + 100

        message = 'Gracias por hacernos saber tu opinión, Es muy importante para nosotros. ¡Obtuviste 100 puntos!'

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')

@login_required
@require_POST 
def cancelar_servicio(request):
    if request.method == 'POST':

        codigo = request.POST.get('codigo', None)
        solicitud = get_object_or_404(Solicitud, codigo=codigo)
        solicitud.fecha_fin = datetime.now()
        solicitud.estado = 'Solicitud Cerrada'
        solicitud.save()

        message = 'Gracias, servicio ha terminado satisfactoriamente.'

    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')