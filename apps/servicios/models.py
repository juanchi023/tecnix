from django.db import models
from django.template.defaultfilters import slugify
from apps.clientes.models import Cliente
from apps.tecnicos.models import Tecnico
from apps.smart_selects.db_fields import ChainedForeignKey
import uuid

class Paquete(models.Model):
	nombre = models.CharField(max_length=120)
	slug = models.SlugField(editable = True, unique=True, null=True, blank=True, max_length=255)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(Paquete, self).save(*args,**kwargs)

	def __str__(self):
		return "{0}".format(self.nombre)

	class Meta:
		verbose_name = "Paquete"
		verbose_name_plural = "Paquetes"

class Servicio(models.Model):
	nombre = models.CharField(max_length=140, blank=False)
	icono = models.CharField(max_length=30, blank=False)
	codigo = models.CharField(max_length=5, blank=False)
	descripcion = models.TextField()
	paquete = models.ForeignKey(Paquete,blank=True)
	precio = models.BigIntegerField(blank=False,null=False)
	se_muestra = models.BooleanField(default=True)


	def __str__(self):
		return "{0} - {1}".format(self.nombre,self.precio)

	class Meta:
		verbose_name = "Servicio"
		verbose_name_plural = "Servicios"
		
class Solicitud(models.Model):

	codigo = models.CharField(max_length=8, blank=True, null=True )
	cliente = models.ForeignKey(Cliente, null=True)
	tecnico = models.ForeignKey(Tecnico,blank=True, null=True)
	direccion = models.CharField(max_length=200, blank=False, null=False )
	telefono = models.BigIntegerField(blank=False, null=False)
	direccion_completa = models.CharField(max_length=200, blank=False, null=False )
	fecha_solicitado = models.DateTimeField(auto_now_add=True)
	fecha_inicio = models.DateTimeField(auto_now_add=False,blank=True, null=True)
	fecha_fin = models.DateTimeField(auto_now_add=False,blank=True, null=True)
	
	NUEVA='Nueva'
	ESPERA='En Espera'
	ASIGNADO= 'Tecnico Asignado'
	CERRADA= 'Solicitud Cerrada'
 
	estado1 = (
		(NUEVA,'Nueva'),
		(ESPERA,'En Espera'),
		(ASIGNADO, 'Tecnico Asignado'),
		(CERRADA, 'Solicitud Cerrada'),
		)
	estado = models.CharField(max_length=20,choices=estado1,default= NUEVA)
	paquete = models.ForeignKey(Paquete,blank=True)
	servicio = ChainedForeignKey(
        Servicio, 
        chained_field="paquete",
        chained_model_field="paquete", 
        show_all=False, 
        auto_choose=False
        )
	total = models.BigIntegerField(blank=True, null=True)
	observaciones = models.TextField(blank=True, null=True)
	tipo_dispositivo = models.CharField(max_length=20, blank=True, null=True )
	num_equipos = models.IntegerField(default=1)


	def save(self, *args, **kwargs):
		self.total = (self.num_equipos * self.servicio.precio) + 20000
		if not self.codigo:
			self.codigo = uuid.uuid4().hex[:8]
		super(Solicitud, self).save(*args,**kwargs)


	def __str__(self):
		return "{0} {1}, {2}".format(self.paquete, self.servicio, self.direccion)

	class Meta:
		verbose_name = "Solicitud"
		verbose_name_plural = "Solicitudes"