from django.conf.urls import include, url
from apps.servicios import views as servicios_views


urlpatterns = [

	url(r'^panel/solicitud-servicio/$', servicios_views.create_solicitud, name='crear_solicitud'),
	url(r'^panel/solicitudes/$', servicios_views.Listarsolicitudes.as_view(), name='solicitudes'),
	url(r'^panel/solicitudes/servicio/(?P<codigo>[-\w]+)/$', servicios_views.SolicitudDetailView
  	, name='detalle_solicitud'),

	url(r'^confirmar-llegada/$', servicios_views.confirmar_llegada_tecnico
  	, name='confirmar_llegada_tecnico'),
	url(r'^confirmar-fin-servicio/$', servicios_views.confirmar_fin_servicio
  	, name='confirmar_fin_servicio'),
	url(r'^tomar-servicio/$', servicios_views.tomar_servicio
  	, name='tomar_servicio'),
	url(r'^valor-servicio/$', servicios_views.valor_servicio
    , name='valor_servicio'),
	url(r'^calificar-servicio/$', servicios_views.calificar_servicio
    , name='calificar_servicio'),
    url(r'^cancelar-servicio/$', servicios_views.cancelar_servicio
  	, name='cancelar_servicio'),
	
	
]
