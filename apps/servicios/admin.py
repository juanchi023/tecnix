from django.contrib import admin
from .models import Servicio, Solicitud, Paquete

class SolicitudAdmin(admin.ModelAdmin):
    readonly_fields=('total',)

admin.site.register(Servicio)
admin.site.register(Solicitud, SolicitudAdmin)
admin.site.register(Paquete)