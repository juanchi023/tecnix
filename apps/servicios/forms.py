from __future__ import unicode_literals
from django.forms import ModelForm
from django import forms
from .models import Solicitud


class create_solicitud_fom(forms.ModelForm):
	
	class Meta:
		model = Solicitud
		exclude = ('fecha_inicio','fecha_fin','estado','total','fecha_solicitado'
			,'fecha_solicitado','cliente','tecnico','codigo')
