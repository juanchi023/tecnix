from django.db import models
from django.contrib.auth.models import User



def upload_imagen_to(instance, filename):
    import os
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    return 'fotos-usuarios/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(), 
    )

class Cliente(User):

    cedula = models.CharField(max_length=20, blank=True)
    telefono1 = models.CharField(max_length=14, blank=True)
    fecha_nacimiento = models.DateField(null=True)
    direccion = models.CharField(max_length=150,blank=True)
    referencia_dir = models.CharField(max_length=140, blank=True)
    puntos = models.IntegerField(default=0,null=True)
    foto = models.ImageField(upload_to=upload_imagen_to, blank=True)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'


    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)