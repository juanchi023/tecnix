from django import forms
from django.contrib.auth import authenticate 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Cliente

class ClienteRegistroForm(UserCreationForm):

    	
    email = forms.EmailField()

    def clean_email(self):
        data = self.cleaned_data['email']
        

        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Este email ya fue registrado por otro usuario")

        return data
    
  

    class Meta:
        model = Cliente
        fields = ('username',
            'first_name',
            'last_name',
            'email',
            )
class ClienteProfileForm(forms.ModelForm):
    fecha_nacimiento=forms.DateField(widget=forms.widgets.DateInput(format="%m-%d-%Y"), input_formats=["%m-%d-%Y"])

    class Meta:
        model = Cliente
        exclude = (
            "puntos",
            "password",
            "last_login",
            "groups",
            "user_permissions",
            "is_staff",
            "is_active",
            "is_superuser",
            "date_joined",)

    def __init__(self, *args, **kwargs):
        super(ClienteProfileForm, self).__init__(*args, **kwargs)