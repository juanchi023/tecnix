from django.conf.urls import include, url
from apps.clientes import views as clientes_views

urlpatterns = [
	url(r'^registrar-cliente/$', clientes_views.registro_clientes, name='register_cliente'),
	url(r'^panel/perfil/editar/$', clientes_views.ClienteUpdateView.as_view(), name='edit_perfil_cliente'),
	
	
  
]