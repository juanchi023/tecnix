from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from .models import Cliente
from .forms import ClienteRegistroForm, ClienteProfileForm
from django.contrib.auth import login,logout
from django.views.decorators.http import require_POST
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from tecnix.estandar.view import InstanceMixin, LoginMixin
from django.core.urlresolvers import reverse_lazy
try:
    from django.utils import simplejson as json
except ImportError:
    import json


@require_POST
def registro_clientes(request):
	if request.method == 'POST' and request.is_ajax():
		form = ClienteRegistroForm(request.POST or None)
		if form.is_valid():
			username = form.cleaned_data['username']
			print(username)
			form.save()
			message = 'Su solicitud se ha registrado! Nos comunicaremos con ud. en breve.'
			ctx = {'message': message}
		else:
			message = form.errors.as_data()
			ctx = {'message': message}

	return HttpResponse(json.dumps(ctx), content_type='application/json')
	
class ClienteUpdateView(UpdateView, LoginMixin):
    template_name = "app/editar-perfil-clientes.html"
    model = Cliente
    form_class = ClienteProfileForm
    success_url = reverse_lazy('profile')

    def get_object(self):
    	return self.request.user.cliente

		