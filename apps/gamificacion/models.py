from django.db import models
from apps.clientes.models import Cliente
from apps.servicios.models import Solicitud, Servicio
from apps.tecnicos.models import Tecnico

class Calificacion(models.Model):
	cliente =  models.ForeignKey(Cliente)
	solicitud = models.ForeignKey(Solicitud)
	tecnico = models.ForeignKey(Tecnico)
	fecha = models.DateTimeField(auto_now_add=True)
	puntuacion = models.IntegerField(default=1)
	comentario = models.TextField(blank=True)

	def __str__(self): 
		return "{0} - {1} - {2}".format(self.puntuacion,self.solicitud,self.tecnico)
