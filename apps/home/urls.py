from django.conf.urls import include, url
from apps.home import views as home_views

urlpatterns = [
	url(r'^$', home_views.home, name='home'),
	url(r'^', include('apps.clientes.urls')),
	url(r'^', include('apps.servicios.urls')),
	url(r'^', include('apps.blog.urls')),
	url(r'^registrar-llamada/$', home_views.registrar_solicitud_llamada, name='registrar_solicitud_llamada'),
	url(r'^login/$', home_views.login_view, name='login_view'),
	url(r'^logout/$', home_views.logout_view, name='logout_view'),
	url(r'^registro/$', home_views.register_view, name='register_view'),
	url(r'^', include('social.apps.django_app.urls', namespace='social')),
	url(r'^panel/$', home_views.panel, name='panel'),
	url(r'^panel/perfil/$', home_views.profile, name='profile'),
	
  
]