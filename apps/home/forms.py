# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import authenticate 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from apps.home.models import Llamada,Suscriptor


class LlamadaForm(forms.ModelForm):
    class Meta:
        model = Llamada
        exclude = ('fecha',)

class SuscriptorForm(forms.ModelForm):
    class Meta:
        model = Suscriptor
        exclude = ('fecha',)

class EmailAuthenticationForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput,required=True)
    

    def __init__(self, *args, **kwargs):
        self.user_cache = None
        super(EmailAuthenticationForm, self).__init__(*args,**kwargs)

    def clean(self):

        self.cleaned_data = super(EmailAuthenticationForm, self).clean()


        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        print (email,password)

        self.user_cache = authenticate(email=email, password=password)

        if self.user_cache is None:
            self._errors["email"] = self.error_class(['Usuario o clave Incorrecta'])
        elif not self.user_cache.is_active:
            self._errors["email"] = self.error_class(['Usuario Inactivo'])
        

        return self.cleaned_data

    def get_user(self):
        return self.user_cache

class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Email O Nombre de usuario"), max_length=254)

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2