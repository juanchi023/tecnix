from django.db import models







class Llamada(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    nombre = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    telefono = models.CharField(max_length=15)

    def __str__(self):
        return "{0} {1} {2}".format(self.nombre, self.telefono, self.fecha)


class Suscriptor(models.Model):
	fecha = models.DateTimeField(auto_now_add=True)
	nombre = models.CharField(max_length=100)
	email = models.CharField(max_length=100)

	def __str__(self):
		return "{0}/{1}/{2}".format(self.nombre, self.email, self.fecha)