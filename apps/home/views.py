from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from .forms import LlamadaForm, EmailAuthenticationForm
from .models import Llamada
from apps.blog.models import Noticia
from apps.tecnicos.models import Tecnico, skill
from apps.servicios.models import Solicitud 
from django.contrib.auth import login,logout
from django.views.decorators.http import require_POST
from apps.clientes.forms import ClienteRegistroForm
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.contrib.auth.decorators import login_required



def home(request):
	return render(request,'home/index.html')

@require_POST 
def registrar_solicitud_llamada(request):
	form = Llamada()
	if request.method == 'POST':
		items = request.POST.get('llamada_dict', None)
		items = str(items).split("/")

		for i in range(0, len(items)):
			item = json.loads(items[i])
			nombre = item['nombre']
			telefono = item['telefono']
			email = item['email']
		form.nombre = nombre
		form.telefono = telefono
		form.email = email
		form.save()
		message = 'Su solicitud se ha registrado! Nos comunicaremos con ud. en breve.'
		ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
	return HttpResponse(json.dumps(ctx), content_type='application/json')


def login_view(request):

	if not request.user.is_anonymous():
		return HttpResponseRedirect(reverse('panel'))
	else:
		form = EmailAuthenticationForm(request.POST or None)
	
		if form.is_valid():
			login(request, form.get_user())
			return HttpResponseRedirect(reverse('panel'))

	return render(request,'app/login.html',{'form':form})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('login_view'))


def register_view(request):
	
	return render(request,'app/register.html')

@login_required
def panel(request):
	noticias = Noticia.objects.all().order_by('-created')[:4]
	try:
		servicios = Solicitud.objects.filter(cliente=request.user.cliente).order_by('-fecha_solicitado')[:5]
		
		return render(request,'app/panel.html', {'servicios':servicios,'noticias':noticias})
	except:
		try:
			solicitudes = Solicitud.objects.filter(estado='Nueva').order_by('fecha_solicitado')
			servicios = Solicitud.objects.filter(tecnico=request.user.tecnico)

			return render(request,'app/panel.html', {'servicios':servicios
			,'solicitudes':solicitudes,'noticias':noticias})
		except:
			return render(request,'app/panel.html',{'noticias':noticias})
	
@login_required
def profile(request):
	return render(request, 'app/profile.html')