$(document).ready(function() {
  /* Panel */
  
  function fixSidebar() {
    var window_height = $(document).height();
    $('#panel aside').css({
      'height': window_height
    });
  }
  setInterval(fixSidebar, 0);
  
  /* Menu */
  
  $('.toggle-menu').click(function(e) {
    $('aside').animate({
      'display': 'block',
      'left': '0'
    }, 200);
    
    $('.opaque').toggle();
    
    e.preventDefault()
  });
  
  $('.opaque').click(function(e) {
    $('aside').animate({
      'display': 'none',
      'left': '-214px'
    }, 200);
    
    $('.opaque').css({
      'display': 'none'
    });
    
    e.preventDefault()
  });
});