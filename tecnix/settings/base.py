import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


SECRET_KEY = 'u9*=if7kb7cg_=#-(fq=l4^&^#)8_%1c28)5ritm$0b&v6qcws'

LANGUAGE_CODE = 'es-co'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True 

WSGI_APPLICATION = 'tecnix.wsgi.application'

DJANGO_APP =(
    'django.contrib.humanize',
	'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

	)

THRID_PARTY_APPS=(
    'widget_tweaks',
    'rest_framework',
    'rest_framework.authtoken',
    'password_reset',
    'social.apps.django_app.default',
    'apps.smart_selects',
    'storages',
    'celery',
    
	)

LOCAL_APPS=(
    'apps.home',
    'apps.clientes',
    'apps.tecnicos',
    'apps.servicios',
    'apps.gamificacion',
    'apps.blog',
	)

INSTALLED_APPS = DJANGO_APP + THRID_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tecnix.urls'



TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '../templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'tecnix.estandar.backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'social.backends.facebook.FacebookAppOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    )
#configuraciones para login con redes sociales

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.mail.mail_validation',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'apps.clientes.pipelines.get_avatar',
    'apps.clientes.pipelines.send_email',
)

LOGIN_URL = '/login/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/panel/'
SOCIAL_AUTH_USER_MODEL = 'clientes.Cliente'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'es_CO',
  'fields': 'id, name, email, age_range' 
}
SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'

SOCIAL_AUTH_FACEBOOK_KEY = '1744428682476709'
SOCIAL_AUTH_FACEBOOK_SECRET = '0f6cb6d6366d4599d42a3d5874da7b9f'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '348572144889-6v6c71scomr3ou0qap08cg8q6bjs7jfj.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '7FdfizuuRmdcTLFpghwJ8gpm'

#configuraciones para enviar emails
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'tecnix'
EMAIL_HOST_PASSWORD = 'Cartagena@023'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

SITE_NAME='Tecnix'

EMAIL_BACKEND = "sgbackend.SendGridBackend"
SENDGRID_API_KEY = "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 "

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_S3_SECURE_URLS = False       # use http instead of https
AWS_QUERYSTRING_AUTH = False     # don't add complex authentication-related query parameters for requests
AWS_S3_ACCESS_KEY_ID = 'AKIAJSMU3YKGNP6UCPZA'     # enter your access key id
AWS_S3_SECRET_ACCESS_KEY = 'r+1t2yfhI+9C89gDBHYqmIiTwaWGP5ZS72+1Cvm2' # enter your secret access key
AWS_STORAGE_BUCKET_NAME = 'tecnix-bucket'

CELERY_ACCEPT_CONTENT = ['pickle','json','msgpack','yaml']
CELERY_TIMEZONE = TIME_ZONE
