from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dego8g3ijppp75',
        'USER' : 'dodsjbusjcvrmv',
        'PASSWORD' : 'Dd_G14Xs2689BhOaDFof_aTA-o',
        'HOST' : 'ec2-184-72-240-189.compute-1.amazonaws.com',
        'PORT' : '5432',
    }
}

STATIC_ROOT = 'staticfile'
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, '../static'),
]

BROKER_URL = 'redis://h:p4su26lk85p26c95vno66m7uur3@ec2-107-20-255-37.compute-1.amazonaws.com:24449'
CELERY_RESULT_BACKEND = 'redis://h:p4su26lk85p26c95vno66m7uur3@ec2-107-20-255-37.compute-1.amazonaws.com:24449'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE