from django.conf.urls import url, include
from django.contrib import admin
from apps.home import reset_password_views as recovery_pass
from django.contrib.auth import views as auth_views


urlpatterns = [
    
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.home.urls')),
    url(r'^', include('apps.tecnicos.urls')),
    url(r'^', include('apps.api.urls')),
    url(r'^', include('password_reset.urls')),
    url(r'^chaining/', include('apps.smart_selects.urls')),
    url(r'^password_reset/$'
    	, recovery_pass.ResetPasswordRequestView.as_view()
    	, name='reset_password'),

    url(r'^password_reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$'
    	, recovery_pass.PasswordResetConfirmView.as_view()
    	,name='reset_password_confirm'),
    
    url(r'^password_reset/enviado/$'
    	, auth_views.password_reset_done
    	, name='password_reset_done'),


    url(r'^password_reset/confirm/terminado/$'
    	,auth_views.password_reset_complete
    	, name='password_reset_complete'),

    
]